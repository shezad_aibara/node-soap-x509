"use strict";

var ursa = require('ursa');
var SignedXml = require('xml-crypto').SignedXml;
var ejs = require('ejs');
var path = require('path');
var fs = require('fs');

function addMinutes( date, minutes ){
  return new Date(date.getTime() + minutes * 60000);
}

function dateStringForSOAP( date ){
  return date.getUTCFullYear() + '-' + ('0' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
    ('0' + date.getUTCDate()).slice(-2) + 'T' + ('0' + date.getUTCHours()).slice(-2) + ":" +
    ('0' + date.getUTCMinutes()).slice(-2) + ":" + ('0' + date.getUTCSeconds()).slice(-2) + "Z";
}

function generateCreated(){
    return dateStringForSOAP(new Date());
  }

function generateExpires(){
  return dateStringForSOAP(addMinutes(new Date(), 10));
}

function insertStr( src, dst, pos ){
  return [dst.slice(0, pos), src, dst.slice(pos)].join('');
}

function WSSecurityCert( privatePEM, publicP12PEM, password, encoding ){
  this._privateKey = ursa.createPrivateKey(privatePEM, password, encoding);
  this._publicP12PEM = publicP12PEM;
  this._signer = new SignedXml();
  this._signer.signingKey = this._privateKey.toPrivatePem();
  
  var references = [ "http://www.w3.org/2000/09/xmldsig#enveloped-signature",
    "http://www.w3.org/2001/10/xml-exc-c14n#" ];
  
  this._signer.addReference("//*[local-name(.)='Body']", references);
  this._signer.addReference("//*[local-name(.)='Timestamp']", references);
  
  this._signer.keyInfoProvider = {};
  this._signer.keyInfoProvider.getKeyInfo = function(key){
    return '<wsse:SecurityTokenReference>' +
      '<wsse:Reference URI="#x509cert00" ' +
      'ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3"/>' +
      '</wsse:SecurityTokenReference>';
  };
  
  var content = fs.readFileSync(path.join(__dirname, 'templates', 'wsse-security-header.ejs'));
  this._template = ejs.compile(content.toString());
}

WSSecurityCert.prototype.postProcess = function( xml ){
    var secHeader = this._template({
        BinaryToken: this._publicP12PEM,
        Created: generateCreated(),
        Expires: generateExpires()
      });
    
    var xmlWithSec = insertStr(secHeader, xml, xml.indexOf('</soap:Header>'));
    
    this._signer.computeSignature(xmlWithSec);
    
    return insertStr(this._signer.getSignatureXml(), xmlWithSec, xmlWithSec.indexOf('</wsse:Security>'));
  };

module.exports = WSSecurityCert;